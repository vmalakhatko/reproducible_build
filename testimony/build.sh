#!/bin/bash -xe

. ../utils.sh

VERSION=1.0

readonly SURICATA_TAR_PATH="${RPM_SOURCES}/suricata-${VERSION}.tar.gz"

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

# For `testimony` version testimony and libhtp should be downloaded separately.
# For other versions libhtp is already included into an archive.

#load_repo "https://github.com/anakorenko/testimony.git" "/root/go/src/github.com/google/testimony" "${TESTIMONY_COMMIT}" dev

if [[ -z "${LOCAL_BUILD}" ]]; then
    git clone -b dev "${TESTIMONY_GIT}" "/root/go/src/github.com/google/testimony"
fi

export PATH=$PATH:/usr/local/go/bin

source ~/.bash_profile

cp /root/testimony/testimony.yaml /root/go/src/github.com/google/testimony/
cd /root/go/src/github.com/google/testimony/
make
mkdir -p build

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog
export SOURCE_DATE_EPOCH="$(head -1 "${RPM_BUILD}/suricata/ChangeLog" | rev | cut -d' ' -f1 | rev | date -f- +%s)"
export SURICATA_CONFIGURE_FLAGS="${BUILD_ARGS_TAIL[@]}"

build_go_rpm testimony.yaml testimony.1.0.x86_64.rpm