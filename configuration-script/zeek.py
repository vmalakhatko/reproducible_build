import json
import tools
import configparser
import sys
import os

config_file_name = 'node.cfg'
config_default_path = '/etc/'

default_configuration = {
    "logger": {
        "type": "logger",
        "host": "localhost"
    },
    "manager": {
        "type": "manager",
        "host": "localhost"
    },
    "proxy-1": {
        "type": "proxy",
        "host": "localhost"
    },
    "worker-1": {
        "type": "worker",
        "host": "localhost",
        "interface":"testimony::/tmp/testimony.sock",
        "lb_method":"custom",
        "lb_procs":1,
        "env_vars":"TESTIMONY_FANOUT_ID=0"
    }
}

class ZeekConfig:
    
    def generate_zeek_agent_config(self, config_file, server_address, server_port, log_folder, max_queued_row_count, osquery_extensions_socket):
        config = {}

        config['server_address'] = server_address
        config['server_port'] = server_port
        config['log_folder'] = log_folder
        config['max_queued_row_count'] = max_queued_row_count
        config['osquery_extensions_socket'] = osquery_extensions_socket
        config['group_list'] = []
        zeek_agent_config = config
        print("Generated config({}): \n {}".format(config_file, json.dumps(zeek_agent_config, indent=4)))

        try:
            with open(config_file, 'w') as outfile:
                json.dump(zeek_agent_config, outfile)
            
        except PermissionError:
            print(f"{tools.bcolors.FAIL}You don't have permission to create or edit file {config_file} {tools.bcolors.ENDC}")


    def generate_config_file(self, config_file, lb_procs, pin_cpus, testimony_socket, fanout_size, disable_logger):
        # check if pinned cpus are defined for every worker 
        
        pin_cpus_list = pin_cpus.split('/')
        if len(pin_cpus) != 0:
            if len(pin_cpus_list) != fanout_size:
                print(f"{tools.bcolors.FAIL} Pinned cpus are not defined for every worker \n Please, define in a format 1/2/3/4 {tools.bcolors.ENDC}")
                sys.exit(0)

        write_config = configparser.ConfigParser()
        if disable_logger not in {"True", "yes"}:
            write_config['logger'] = default_configuration['logger']
        write_config['manager'] = default_configuration['manager']
        write_config['proxy-1'] = default_configuration['proxy-1']
        
        for i in range(0, fanout_size):
            manager_name = "worker-" + str(i + 1)
            write_config[manager_name] = {}
            write_config[manager_name]['type'] = 'worker'
            write_config[manager_name]['host'] = 'localhost'
            write_config[manager_name]['interface'] = "testimony::" + testimony_socket
            write_config[manager_name]['lb_method'] = 'custom'
            write_config[manager_name]['lb_procs'] = str(lb_procs)
            if len(pin_cpus) != 0:
                write_config[manager_name]['pin_cpus'] = pin_cpus_list[i]
            write_config[manager_name]['env_vars'] = 'TESTIMONY_FANOUT_ID=' + str(i)
            
        print("Generated config({}): \n".format(config_file))
        write_config.write(sys.stdout)

        try:
            with open(config_file, 'w') as outfile:
                write_config.write(outfile)
            os.system("cp " + os.getcwd() + "/zeek.service /usr/lib/systemd/system/")
            os.system("systemctl daemon-reload")
            os.system("systemctl enable zeek")
            os.system("systemctl start zeek")
        except PermissionError:
            print(f"{tools.bcolors.FAIL}You don't have permission to create or edit file {config_file} {tools.bcolors.ENDC}")

