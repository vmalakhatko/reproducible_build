#!/usr/bin/python3

import testimony
import stenographer
import suricata
import zeek
import tools
import sys
import getopt
import argparse


def generate_testimony_config(args):
    testimony.TestimonyConfig().generate_config_file(config_file=args.testimony_file_path, 
                                                        socket_name=args.socket_name, interface=args.interface,
                                                        block_size=args.block_size, frame_size=args.frame_size, num_blocks=args.num_blocks,
                                                        fanout_size=args.fanout_size, block_timeout_millis=args.block_timeout_millis, number_of_clients=args.number_of_clients,
                                                        user=args.user, interface_config=args.interface_config)

def generate_stenographer_config(args):
    stenographer.StenographerConfig().generate_config_file(config_file = args.stenographer_file_path, 
                                                            stenotype_path=args.stenotype_path,
                                                            interface=args.interface,
                                                            testimony_socket=args.testimony_socket,
                                                            port=args.port, host=args.host, 
                                                            cert_path=args.cert_path, 
                                                            fanout_size=args.fanout_size, 
                                                            packet_index_dir=args.packet_index_dir,
                                                            uid=args.uid, gid=args.gid,
                                                            max_packet_files=args.max_packet_files, 
                                                            disk_percentage=args.disk_percentage)

def generate_zeek_config(args):
    zeek.ZeekConfig().generate_config_file(config_file=args.zeek_file_path,
                                            lb_procs=args.lb_procs, pin_cpus=args.pin_cpus,
                                            testimony_socket=args.testimony_socket, 
                                            fanout_size=args.fanout_size, 
                                            disable_logger=args.disable_logger)

def generate_zeek_agent_config(args):
    zeek.ZeekConfig().generate_zeek_agent_config(config_file=args.zeek_agent_file_path, server_address=args.server_address, server_port=args.server_port, log_folder=args.log_folder,
                                        max_queued_row_count = args.max_queued_row_count, osquery_extensions_socket = args.osquery_extensions_socket)

def generate_suricata_config(args):
    suricata.SuricataConfig().generate_config_file(config_file=args.suricata_file_path, 
                                                    testimony_lib=args.testimony_lib, stenographer_lib=args.stenographer_lib,
                                                    testimony_socket=args.testimony_socket, fanout_size=args.fanout_size,
                                                    filename=args.filename, pcap_dir=args.pcap_dir, cert_dir=args.cert_dir,
                                                    command_pipe=args.command_pipe, before_time=args.before_time, after_time=args.after_time,
                                                    compression=args.compression, no_overlapping=args.no_overlapping, clean_up=args.clean_up, cleanup_script=args.cleanup_script,
                                                    expiry_time=args.expiry_time, min_disk_space_left=args.min_disk_space_left)

def main(args):
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    parser_testimony = subparsers.add_parser('testimony', help='Generate testimony configuration file')
    parser_stenographer = subparsers.add_parser('stenographer', help='Generate stenographer configuration file')
    parser_zeek = subparsers.add_parser('zeek', help='Generate zeek configuration file')
    parser_zeek_agent = subparsers.add_parser('zeek-agent', help='Generate zeek-agent configuration file')
    parser_suricata = subparsers.add_parser('suricata', help='Generate suricata configuration file')
    
    # Testimony args
    parser_testimony.add_argument('--testimony_file_path', action='store', type=str, default='/etc/nsm/testimony.conf')
    parser_testimony.add_argument('--socket_name', action='store', type=str, default='/tmp/testimony.sock')
    parser_testimony.add_argument('--interface', action='store', type=str, default='eth0')
    parser_testimony.add_argument('--block_size', action='store', type=int, default=1048576)
    parser_testimony.add_argument('--frame_size', action='store', type=int, default=16384)
    parser_testimony.add_argument('--num_blocks', action='store', type=int, default=16)
    parser_testimony.add_argument('--fanout_size', action='store', type=int, default=1)
    parser_testimony.add_argument('--block_timeout_millis', action='store', type=int, default=1000)
    parser_testimony.add_argument('--number_of_clients', action='store', type=int, default=3)
    parser_testimony.add_argument('--user', action='store', type=str, default='stenographer')
    parser_testimony.add_argument('--interface_config', action='store', type=str, default='no')
    parser_testimony.set_defaults(func=generate_testimony_config)

    # Stenographer args
    parser_stenographer.add_argument('--stenographer_file_path', action='store', type=str, default='/etc/nsm/stenographer/config')
    parser_stenographer.add_argument('--stenotype_path', action='store', type=str, default='/usr/bin/stenotype')
    parser_stenographer.add_argument('--testimony_socket', action='store', type=str, default='')
    parser_stenographer.add_argument('--interface', action='store', type=str, default='')
    parser_stenographer.add_argument('--port', action='store', type=int, default=1234)
    parser_stenographer.add_argument('--host', action='store', type=str, default='127.0.0.1')
    parser_stenographer.add_argument('--cert_path', action='store', type=str, default='/etc/nsm/stenographer/certs')
    parser_stenographer.add_argument('--fanout_size', action='store', type=int, default=1)
    parser_stenographer.add_argument('--packet_index_dir', action='store', type=str, default='/data/stenographer/')
    parser_stenographer.add_argument('--uid', action='store', type=str, default='root')
    parser_stenographer.add_argument('--gid', action='store', type=str, default='root')
    parser_stenographer.add_argument('--max_packet_files', action='store', type=int, default=30000)
    parser_stenographer.add_argument('--disk_percentage', action='store', type=int, default=1)
    parser_stenographer.set_defaults(func=generate_stenographer_config)

    # Zeek args
    parser_zeek.add_argument('--zeek_file_path', action='store', type=str, default='/etc/nsm/zeek/node.cfg')
    parser_zeek.add_argument('--testimony_socket', action='store', type=str, default='/tmp/testimony.sock')
    parser_zeek.add_argument('--lb_procs', action='store', type=int, default=1)
    parser_zeek.add_argument('--pin_cpus', action='store', type=str, default='')
    parser_zeek.add_argument('--fanout_size', action='store', type=int, default=1)
    parser_zeek.add_argument('--disable_logger', action='store', type=str, default='no')
    parser_zeek.set_defaults(func=generate_zeek_config)

    # Zeek-agent args
    parser_zeek_agent.add_argument('--zeek_agent_file_path', action='store', type=str, default='/etc/zeek-agent/config.json')
    parser_zeek_agent.add_argument('--server_address', action='store', type=str, default='127.0.0.1')
    parser_zeek_agent.add_argument('--server_port', action='store', type=int, default=9999)
    parser_zeek_agent.add_argument('--log_folder', action='store', type=str, default='/var/log/zeek')
    parser_zeek_agent.add_argument('--max_queued_row_count', action='store', type=int, default=5000)
    parser_zeek_agent.add_argument('--osquery_extensions_socket', action='store', type=str, default='/var/osquery/osquery.em')
    parser_zeek_agent.set_defaults(func=generate_zeek_agent_config)

    # Suricata args
    parser_suricata.add_argument('--suricata_file_path', action='store', type=str, default='/etc/nsm/suricata/suricata.yaml')
    parser_suricata.add_argument('--testimony_lib', action='store', type=str, default='/usr/local/lib/source-testimony.so')
    parser_suricata.add_argument('--stenographer_lib', action='store', type=str, default='/usr/local/lib/eve-stenographer.so')
    
    parser_suricata.add_argument('--testimony_socket', action='store', type=str, default='/tmp/testimony.sock')
    parser_suricata.add_argument('--fanout_size', action='store', type=int, default=1)

    parser_suricata.add_argument('--filename', action='store', type=str, default='/var/log/stenographer.log')
    parser_suricata.add_argument('--pcap_dir', action='store', type=str, default='/tmp/pcaps/')
    parser_suricata.add_argument('--command_pipe', action='store', type=str, default='/tmp/suricata-stenographer.sock')
    parser_suricata.add_argument('--cert_dir', action='store', type=str, default='/etc/nsm/stenographer/certs/')
    parser_suricata.add_argument('--before_time', action='store', type=int, default=30)
    parser_suricata.add_argument('--after_time', action='store', type=int, default=5)
    parser_suricata.add_argument('--compression', action='store', type=str, default='no')
    parser_suricata.add_argument('--no_overlapping', action='store', type=str, default='no')
    parser_suricata.add_argument('--clean_up', action='store', type=str, default='no')
    parser_suricata.add_argument('--cleanup_script', action='store', type=str, default='/home/script.sh')
    parser_suricata.add_argument('--expiry_time', action='store', type=int, default=0)
    parser_suricata.add_argument('--min_disk_space_left', action='store', type=int, default=0)
    parser_suricata.set_defaults(func=generate_suricata_config)


    try:
        args = parser.parse_args(args)
        args.func(args)
    except AttributeError:
        print(f"{tools.bcolors.FAIL}Please, run --help command {tools.bcolors.ENDC}")
    
    

if __name__ == "__main__":
    main(sys.argv[1:])
