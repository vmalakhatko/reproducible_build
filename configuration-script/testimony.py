import json
import tools
import os

default_config_file_path = '/etc/testimony.conf'
default_configuration = {
        'SocketName': '/tmp/testimony.sock',
        'Interface': 'eth0', 
        'BlockSize': 1048576,
        'FrameSize': 16384, 
        'NumBlocks': 16, 
        'FanoutSize': 4,
        'BlockTimeoutMillis': 1000,
        'NumberOfClients': 3, 
        'User': 'stenographer'
    }

class TestimonyConfig:
    
    def generate_config_file(self, config_file=default_config_file_path, 
                            socket_name='/tmp/testimony.sock', interface='eth0',
                            block_size=1048576, frame_size = 16384, num_blocks=16, fanout_size=1,
                            block_timeout_millis=1000, number_of_clients=3, user='stenographer', interface_config=False):
        config = {}

        config['SocketName'] = socket_name
        config['Interface'] = interface
        config['BlockSize'] = block_size
        config['FrameSize'] = frame_size
        config['NumBlocks'] = num_blocks
        config['FanoutSize'] = fanout_size
        config['BlockTimeoutMillis'] = block_timeout_millis
        config['NumberOfClients'] = number_of_clients
        config['User'] = user
        
        testimony_config = [config]
        print("Generated config({}): \n {}".format(config_file, json.dumps(testimony_config, indent=4)))

        try:
            with open(config_file, 'w') as outfile:
                json.dump(testimony_config, outfile)
            
            os.system("cp " + os.getcwd() + "/testimony.service /usr/lib/systemd/system/")
            os.system("systemctl daemon-reload")
            os.system("systemctl enable testimony")
            os.system("systemctl start testimony")

            if interface_config in {"True", "yes"}:
                print("Creating service for auto-configuring interface {}".format(interface))
                os.system("echo '#!/bin/bash\nip link set {} mtu 9000 \nip link set {} promisc on' > /var/tmp/interface_configuration.sh".format(interface, interface))
                os.system("chmod +x /var/tmp/interface_configuration.sh")
                os.system("echo '[Unit] \n Description=Description for sample script goes here \n"
                          "After=network.target\n \n[Service]\nType=simple\nExecStart=/var/tmp/interface_configuration.sh\nTimeoutStartSec=0\n \n"
                          "[Install]\nWantedBy=default.target\n' > /usr/lib/systemd/system/interface_configuration.service")
                os.system("systemctl daemon-reload")
                os.system("systemctl enable interface_configuration.service")
                os.system("systemctl start interface_configuration.service")
            
        except PermissionError:
            print(f"{tools.bcolors.FAIL}You don't have permission to create or edit file {config_file} {tools.bcolors.ENDC}")

