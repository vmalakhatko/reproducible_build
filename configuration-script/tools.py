import os
import socket
import fcntl
import struct
import array

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def all_interfaces():
    max_possible = 128 # arbitrary. raise if needed.
    obytes = max_possible * 32
    deb = b'\0'
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    names = array.array('B', deb * obytes)

    outbytes = struct.unpack('iL', fcntl.ioctl(
        s.fileno(),
        0x8912,  # SIOCGIFCONF
        struct.pack('iL', obytes, names.buffer_info()[0])
        ))[0]
    namestr = names.tostring()
    lst = []
    for i in range(0, outbytes, 40):
        name = namestr[ i: i+16 ].split( deb, 1)[0]
        name = name.decode()
        #iface_name = namestr[ i : i+16 ].split( deb, 1 )[0]
        # ip   = namestr[i+20:i+24]
    lst.append(name)
    return lst

def find_all(name, path):
    result = {}
    counter = 0
    for root, dirs, files in os.walk(path):
        if name in files:
            counter += 1
            result[str(counter)] = os.path.join(root, name)
    return result

def ask_for_value(name, default, message=""):
    print(message)
    result = input("Press Enter to use default value (" + str(default) + ") for " + name + ", or define new " + name + " here\n")
    return default if str(result) == "" else result 