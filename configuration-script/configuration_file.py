#!/usr/bin/python3

import yaml
import argparse
import tools
import configuration

def run_configuration_generation(parsed_yaml_config_file, app_name):
    """
    docstring
    """
    if parsed_yaml_config_file[app_name]:
        configuration_list = [app_name]
        for key, value in parsed_yaml_config_file[app_name].items():
            configuration_list.append('--{}={}'.format(key, value))
        configuration.main(configuration_list)

def main():
    """
    docstring
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('--file_path', action='store', type=str, required=True)
    args = parser.parse_args()

    try:
        with open(args.file_path) as yaml_configuration_file:
            parsed_yaml_config_file = yaml.safe_load(yaml_configuration_file)

            run_configuration_generation(parsed_yaml_config_file, 'testimony')
            run_configuration_generation(parsed_yaml_config_file, 'suricata')
            run_configuration_generation(parsed_yaml_config_file, 'stenographer')
            run_configuration_generation(parsed_yaml_config_file, 'zeek')
            run_configuration_generation(parsed_yaml_config_file, 'zeek-agent')
            
            
    except FileNotFoundError as e:
        print(f"{tools.bcolors.FAIL}Cannot open file: {str(e)} {tools.bcolors.ENDC}")
    except yaml.scanner.ScannerError as e:
        print(f"{tools.bcolors.FAIL}Cannot parse configuration yaml file: {str(e)} {tools.bcolors.ENDC}")



if __name__ == "__main__":
    main()
