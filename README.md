# Table of Contents
1. [Building Suricata, Zeek, Testimony, Stenographer](#Building-Suricata,-Zeek,-Testimony,-Stenographer)
2. [Downloading dependencies](#Downloading-dependencies)
3. [Testing](#Testing)
4. [Troubleshooting](#Troubleshooting)
5. [Configuration](#Configuration)  
    5.1 [Testimony](#Testimony)  
    5.2 [Stenographer](#Stenographer)  
    5.3 [Suricata + testimony plugin](#Suricata-+-testimony-plugin)  
    5.4 [Suricata + stenographer plugin](#Suricata-+-stenographer-plugin)  
    5.5 [Zeek](#Zeek)
6. [Auto-configuration script](#Auto-configuration-script)
7. [Configuration generation with .yaml file](#Configuration-generation-with-.yaml-file)
8. [Configuration Testing](#Configuration-Testing)
9. [Performance increasing script](#Performance-increasing-script)



# Building Suricata, Zeek, Testimony, Stenographer

## Prerequisites

To use build environment, you should have `docker` installed.

## Creating and using build environment

1. Clone this repo and `cd` to the root directory of the repo.
2. Build or import Build System Docker image (see **Importing, exporting and updating Build System Docker image** section).
    1. Import existing image.
        * Run `./build.sh bs-import <image archive>`.
    2. Build new image.
        * Run `./build.sh bs-rebuild <image name>`.
3. Run `./build.sh` with desired options for targets.
4. If the build was successful, you will find RPM(s) in `./out` directory.

## Description of `./build.sh` options for targets

Common invocation format: `./build.sh <TARGET> [...]`.  
`TARGET` is either `suricata`, or `zeek`, or `zt` (Zeek-Testimony plugin), or `stenographer` or `testimony` or `suricata-testimony-plugin` or `suricata-stenographer-plugin`

Formats for targets:  
`./build.sh suricata`  
or  
`./build.sh zeek <VERSION> [CONFIGURE-FLAGS]`  
or  
`./build.sh zt --zeek-rpm=<path> [ZT-FLAGS]`  
or  
`./build.sh testimony`  
or  
`./build.sh stenographer`  
or  
`./build.sh suricata-testimony-plugin`  
or  
`./build.sh suricata-stenographer-plugin`  


Resulted RPM is saved in `${PWD}/out` directory.

### Zeek (`zeek`) target
For `zeek` `VERSION` should always be a valid version code, because Zeek is downloaded as `.tar.gz` from official repo.

### Zeek-Testimony (`zt` target) - `ZT-FLAGS`

This target requires Zeek to be built before. It uses Zeek RPM as a build dependency.

| Flag | Description |
|----- | ----------- |
| `--zeek-rpm=<path>`         | set path to pre-built Zeek RPM package, it is used as the build dependency. Mandatory |
| `--testimony-commit=<hash>` | specify testimony commit. If not set, lates is used |
| `--zt-commit=<hash>`        | specify Zeek-Testimony plugin commit. If not set, latest is used |

### Examples

To build Zeek use this command:
`./build.sh zeek 3.1.1`

To build Zeek-Testimpony plugin use this command:
`./build.sh zt --zeek-rpm=out/zeek-3.1.1-0.x86_64.rpm --testimony-commit=77788166dc886779c3545c689a8a5fb1be8aed29 --zt-commit=4383e44e5ceb22c3386c3fee720ff74f3a18fe4e`.  
**N.B.:** package `./out/zeek-3.1.1-0.x86_64.rpm` should exist.

## Importing, exporting and updating Build System Docker image

For managing Build System image, `./build.sh` has such commands. They are used in format `./build.sh <COMMAND> [...]`.

| Command | Description |
| ------- | ----------- |
| `bs-import <image file>` | Imports Build System from existing image file to Docker (`.tar.gz` archive) |
| `bs-export <image file>` | Exports Build System from Docker to ready-to-use image file (`.tar.gz` archive) |
| `bs-rebuild [--no-cache]` | Creates Build System Docker image from `Dockerfile` (must be in the same directory). `--no-cache` can be used to do full rebuild |

**N.B.:** You must do either `bs-import` or `bs-rebuild` before the first usage of the Build System.

### Examples

Import Build System from existing archive named `reproducible-builder.tar.gz`.  
`./build.sh bs-import reproducible-builder.tar.gz`

Export Build System to archive named `reproducible-builder.tar.gz`.  
`./build.sh bs-export reproducible-builder.tar.gz`

Create new Build System from `Dockerfile`.  
`./build.sh bs-rebuild --no-cache`

# Downloading dependencies

Please, run ./build.sh dependency to download all dependencies 
All dependencies will be downloaded in separate folders (each folder will be created in `/out/` directory):

Testimony - `/out/testimony`
Suricata - `/out/suricata`
Stenographer - `/out/stenographer`
Zeek - `/out/zeek`
Libraries for configuration script running and testing - `/out/testing`
Suricata-stenographer plugin - `/out/suricata-stenographer`


# Testing
Run `./test.sh` and wait. It will build 4 RPMS and create a list of their sha256 hashes (`hashes.list`) in `./out` directory:
    `suricata-5.0.2-0.x86_64.rpm`
    `suricata-testimony-0.x86_64.rpm`
    `zeek-3.1.1-0.x86_64.rpm`
    `zeek-testimony-latest-0.x86_64.rpm`

It will also try to install each RPM in separate CentOS 8 container.
You can try to run the test script on different machines. Hashes should be identical everywhere.

# Build environment structure

| File                     | Description |
| ------------------------ | ----------- |
| `Dockerfile`             | Contains build environment configuration - docker image based on CentOS 8 |
| `build.sh`               | Used to trigger build process (described abode), isn't packed into docker image |
| `entrypoint.sh`          | Docker container entrypoint script. `./build.sh` passes arguments to it |
| `pkg-version.sh`         | Used to find specific package version of desired package |
| `test.sh`                | Contains predefined build commands to cover Suricata and Zeek builds, isn't packed into docker image |
| `utils.sh`               | Common functions and constants for `suricata/build.sh` and `zeek/build.sh` |
| `suricata/build.sh`      | This script is used to build Suricata. Invoked from `entrypoint.sh` |
| `suricata/suricata.spec` | Build process description for `rpmbuild` to build Suricata package |
| `zeek/build.sh`          | This script is used to build Zeek. Invoked from `entrypoint.sh` |
| `zeek/zeek.spec`         | Build process description for `rpmbuild` to build Zeek package |
| `zeek-testimony/build.sh`            | This script is used to build Zeek-Testimony plugin. Invoked from `entrypoint.sh` |
| `zeek-testimony/zeek-testimony.spec` | Build process description for `rpmbuild` to build Zeek-Testimony package |

# Troubleshooting

## Unable to find packages during `./build.sh bs-rebuild`

Sometimes when rebuilding Build System image (via `./build.sh bs-rebuild`) you can see something like:  
```
No match for argument: gcc-8.3.1-4.4.el8
Error: Unable to find a match: gcc-8.3.1-4.4.el8
```

It means that package `gcc-8.3.1-4.4.el8` was removed from CentOS 8 repos. In this case you need to replace it in `Dockerfile` with newer version.  
To do this, run `./pkg-version.sh gcc`. It will show newest package version (e.g. `gcc-8.3.1-4.5.el8`).  
Copy it, open `Dockerfile`, find references to the old package (typically in `RUN yum -y --enablerepo=PowerTools install ...` section) and replace it with new one.

# Configuration

          ## Testimony

          1. Build docker image using command `./build.sh bs-rebuild`  
          2. Run ./build.sh testimony to get rpm with testimonyd app  
          3.  Install syslog-ng using this command:   
          `cd testimony/syslog-ng`    
          `yum -y --nogpgcheck localinstall ivykis-0.42.4-2.el8.x86_64.rpm syslog-ng-3.23.1-2.el8.x86_64.rpm`  
          4. Install rpm using
          `yum -y --nogpgcheck localinstall testimony-***.rpm`  
          5. Testimonyd will be installed to /usr/local/bin/testimonyd:  
          6. Add Testimony config file /etc/testimony.conf with the following content:  
```
[
 {
    "SocketName": "/tmp/testimony.sock"
   , "Interface": "eth0"
   , "BlockSize": 1048576
   , "FrameSize": 16384
   , "NumBlocks": 4
   , "BlockTimeoutMillis": 1000
   , "NumberOfClients": 3,
   , "FanoutSize": 0
   , "User": "stenographer"
 }
]
```

If needed, replace Interface with yours
Run  /usr/local/bin/testimonyd -config /etc/testimony.conf

Added new parameter FrameSize. Number of frames. Frames are grouped in blocks. If FrameSize 
is a divisor of tp_block_size frames will be contiguously spaced by 'FrameSize'
bytes. If not, there will be a gap between the frames in blocks. This is because
a frame cannot be spawn across two blocks.

Added ability to see if client receives packets:
`[S:/tmp/testimony.sock:0] stats: Not sent to client, 2325 packets (38.75pps), 2153 drops (35.88pps) (48.08% dropped) since last log, 2325 packets, 2153 drops total (48.08% dropped)`

## Stenographer  
1. Build docker image using command `./build.sh bs-rebuild`  
2. Run `./build.sh stenographer`  
3. Install rpm using command   
 `sudo rpm -ihv --nodeps stenographer***.rpm`  
4. Run script stenokeys.sh to generate .pem keys for stenographer (stenokeys.sh is in out folder)  
5. Change configuration file:  /etc/stenographer/config:  
```
{
 "Threads": [
   { "PacketsDirectory": "/data/stenographer/PKT0"
   , "IndexDirectory": "/data/stenographer/IDX0"
   , "MaxDirectoryFiles": 30000
   , "DiskFreePercentage": 10
   }
 ]
 , "StenotypePath": "/usr/bin/stenotype"
 , “TestimonySocket”: “/tmp/testimony.sock”
 , "Port": 1234
 , "Host": "127.0.0.1"
 , "Flags": ["--dir=/data/stenographer/"]
 , "CertPath": "/etc/stenographer/certs"
}
```

`PacketsDirectory` and `IndexDirectory` must be valid folders with `stenographer` user as an owner and must have the same root directory (“/data/stenographer/” here as an example):
For example:
sudo chown -R stenographer /data/stenographer
If your PacketsDirectory and IndexDirectory are in `/data/stenographer` folder.

If needed, replace Interface with yours  

Replace “Interface” with “TestimonySocket” if you want to use testimony as a packet source.   
Example:  
```
… ,
“TestimonySocket”: “/tmp/testimony.sock”
 …
```

If testimony is configured with fanout > 1,
Number of threads in stenographer configuration should be the same as “FanoutSize” option in testimony configuration:
Example:
/etc/testimony.conf
```
[
 {
    "SocketName": "/tmp/testimony.sock"
   , "Interface": "eth0"
   , "BlockSize": 1048576
   , "FrameSize": 16384
   , "NumBlocks": 4
   , "BlockTimeoutMillis": 1000
   , "NumberOfClients": 3,
   , "FanoutSize": 4
   , "User": "stenographer"
 }
] 
```

“User” field should be “stenographer” for stenographer to be able to access /tmp/testimony.sock
NumberOfClients option should be 3 to show that 3 clients(stenographer, suricata, zeek) are connected to testimony.

/etc/stenographer/config (number of threads is the same as fanout size):  
```
{
 "Threads": [
   { "PacketsDirectory": "/data/stenographer/PKT0/"
   , "IndexDirectory": "/data/stenographer/IDX0/"
   , "MaxDirectoryFiles": 30000
   , "DiskFreePercentage": 10
   },
{ "PacketsDirectory": "/data/stenographer/PKT1/"
   , "IndexDirectory": "/data/stenographer/IDX1/"
   , "MaxDirectoryFiles": 30000
   , "DiskFreePercentage": 10
   },
{ "PacketsDirectory": "/data/stenographer/PKT2/"
   , "IndexDirectory": "/data/stenographer/IDX2/"
   , "MaxDirectoryFiles": 30000
   , "DiskFreePercentage": 10
   },
{ "PacketsDirectory": "/data/stenographer/PKT3/"
   , "IndexDirectory": "/data/stenographer/IDX3/"
   , "MaxDirectoryFiles": 30000
   , "DiskFreePercentage": 10
   }
 ]
 , "StenotypePath": "/usr/bin/stenotype"
 , "Interface": “eth0”
 , "Port": 1234
 , "Host": "127.0.0.1"
 , "Flags": ["--dir=/data/stenographer/"]
 , "CertPath": "/etc/stenographer/certs"
}
```

Run stenographer using “service stenographer restart”

### Last changes
1. Added testimony plugin for stenotype, now packets and blocks being counted on testimony side. 

## Suricata + testimony plugin

1. Build docker image using command `./build.sh bs-rebuild`  
2. Run `./build.sh suricata-testimony-plugin`   
3. Install rpm using 
`yum -y --nogpgcheck localinstall ***.rpm`

Library ‘source-testimony.so’ will be installed to default library “/usr/lib” or “/usr/local/lib”

4. Add include option to the end of  suricata configuration file plugin (if you want to have separate configuration file for testimony) and create new config file at /etc/suricata_testimony.yaml path:
include: /etc/suricata_testimony.yaml
Add path to testimony plugin library to suricata.yaml file

```
plugins:
  - /other_plugin/plugin.so
  - /path/to/testimony/library/source-testimony.so
 Add testimony configuration to separate file or to suricata.yaml config file
```

```
%YAML 1.1
---
testimony:
  "/tmp/testimony.sock":
    fanout-size: 4
```

Where /tmp/testimony.sock is a path to testimony socket and fanout-size is a fanout size configured in the testimony configuration file.

5. Run suricata using options --capture-plugin and --capture-plugin-args:
./src/suricata -c suricata.yaml --capture-plugin testimony-plugin --capture-plugin-args /tmp/testimony.sock

Where /tmp/testimony.sock is a path to testimony socket
 
## Suricata + stenographer plugin

1. Build docker image using command `./build.sh bs-rebuild`
2. Run `./build.sh suricata-stenographer-plugin` 
3. Install rpm using `yum -y --nogpgcheck localinstall***.rpm`  

library ‘eve-stenographer.so’ will be installed to default library “/usr/lib” or “/usr/local/lib”  

4. Add include option to the end of  suricata configuration file plugin:  

Add path of suricata-stenographer plugins suricata config file at /etc/suricata.yaml or /etc/suricata/suricata.yaml  

```
plugins:
  - /Other/Plugin/plugin.so
  - /Suricata-stenographer-plugin-path/eve-stenographer.so
```

5. Change - eve-log section to enable stenographer config

```
outputs:
  - eve-log:
      enabled: yes
      filetype: stenographer-plugin
      filename: eve.json
      stenographer-plugin:
        enabled: yes
        filename: /var/log/stenographer.log
        pcap-dir: /tmp/pcaps/
        cert-dir: /etc/nsm/stenographer/certs
        command-pipe: /tmp/suricata-stenographer.sock
        before-time: 30
        after-time: 5
        compression: no
        no-overlapping: no
        cleanup:
          enabled: no
          script: /home/vadym/script.sh
          expiry-time: 0
          min-disk-space-left: 4000000000

filename (file with logs),
pcap-dir (directory to save alert pcap files),
before-time, after-time (period of time to save pcaps for alert)
cert-dir (folder with stenographer certs)
command-pipe (named pipe to send commands to plugin for pcap saving)
compression (lz4 compression for pcap files),
no-overlapping (save packets from the same time period), 
cleanup:
 enabled (is pcap directory cleanup enabled):
 script (script to run before cleanup)
 expiry-time (pcap file live time before cleanuo)
 min-disk-space-left (amount of space to be left on device)

Command-pipe can be used to save pcaps using different applications or command line.



```

Now you can specify time measurment units for ```before-time```, ```after-time```, ```expiry-time```  and memory units for ```min-disk-space-left```. For example:
```
      before-time: 30s
      after-time: 5m
      expiry-time: 2h
      min-disk-space-left: 4gb
```| Units | Description | Units | Description |
| :---: | :---: | :---: | :---:|
|```s```|```second```| ```kb```| ```Kilobytes``` |
|```m```|```minutes```|```mb```| ```Megabytes``` |
|```h```|```hours```| ```gb```| ```Gigabytes``` |
|```d```|```days```|
|```w```|```week```|
```
Without any of these specfiers **default values would be** set  - ```s``` for time units and ```kb``` for memory units.

## Zeek

1. Install Zeek and Zeek testimony plugin: 

`yum --nogpgcheck install -y zeek-testimony-**.rpm`

2. Enable EPEL to get syslog-ng 
`yum install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm`

3. Install syslog-ng
`yum install syslog-ng`

4. Change configuration file `node.cfg`
If needed, replace interface (testimony socket path) with yours:
```
[manager]
type=manager
host=localhost

[proxy-1]
type=proxy
host=localhost

[worker-1]
type=worker
host=localhost
interface=testimony::/tmp/testimony.sock
lb_method=custom
lb_procs=1
pin_cpus=1
env_vars=TESTIMONY_FANOUT_ID=0

[worker-2]
type=worker
host=localhost
interface=testimony::/tmp/testimony.sock
lb_method=custom
lb_procs=1
pin_cpus=2
env_vars=TESTIMONY_FANOUT_ID=1
...

```

5. Start syslog-ng
6. Start testimony and zeek: 
`zeekctl deploy`


## Creating conditional pcaps using Suricata-Stenographer plugin 
Suricata creates .pcap files when alerts are detected using suricata-stenographer plugin.
This plugin is also capable of using external named pipe to receive messages to create .pcaps.

Zeek can be used to send messages to named pipe.
Check, please StenographerSuricataRecord folder. There is a zeek script which can be used in your scripts to create .pcap files using suricata-stenographer plugin.

Main function to send message to named pipe is 
`write_to_pipe("/tmp/pipe1", "test");`
where `/tmp/pipe1` is a path to pipe generated by suricata and test is a message (.pcap file will include this message).

### Last changes
1. Secured compatibility for testimony plugin with ZEEK 4.0.0 

# Auto-configuration script

Script for auto configuration is in separate folder, so firstly go to `reproducible_build/configuration-script/` folder:

`cd reproducible_build/configuration-script/`

## Install python requirements:

`cd packages/`

`sudo rpm -i *.rpm`

`pip3 install *.whl`

Now configuration files can be generated/modified:
Run script(using root privileges (sudo) if files have root access permission):


`sudo ./configuration.py --help`

Script can be used to generate configuration files:

```
usage: configuration.py [-h] {testimony,stenographer,zeek,suricata} ...

  {testimony,stenographer,zeek,suricata}
    testimony           Generate testimony configuration file
    stenographer        Generate stenographer configuration file
    zeek                Generate zeek configuration file
    suricata            Generate suricata configuration file

```

### Testimony:
To configure testimony config run 
`./configuration.py testimony ..args..`

```
usage: configuration.py testimony [-h]
                                  [--testimony_file_path TESTIMONY_FILE_PATH]
                                  [--socket_name SOCKET_NAME]
                                  [--interface INTERFACE]
                                  [--block_size BLOCK_SIZE]
                                  [--num_blocks NUM_BLOCKS]
                                  [--fanout_size FANOUT_SIZE]
                                  [--block_timeout_millis BLOCK_TIMEOUT_MILLIS]
                                  [--user USER]
                                  [--interface_config INTERFACE_CONFIG]

optional arguments:
  -h, --help            show this help message and exit
  --testimony_file_path TESTIMONY_FILE_PATH
  --socket_name SOCKET_NAME
  --interface INTERFACE
  --block_size BLOCK_SIZE
  --num_blocks NUM_BLOCKS
  --fanout_size FANOUT_SIZE
  --block_timeout_millis BLOCK_TIMEOUT_MILLIS
  --user USER
  --interface_config INTERFACE_CONFIG
```

Option `interface_config` should be "yes" or "no". If it's "yes" commands will be executed:

```
$ sudo ip link set eth0 mtu 9000
$ ip link set eth0 promisc on
```


Example:  

`sudo ./configuration.py testimony --testimony_file_path=/etc/testimony.conf --socket_name=/tmp/testimony.sock2 --interface=wlp3s0 --block_size=1048576 --num_blocks=16 --fanout_size=1 --block_timeout_millis=100 --user=stenographer --interface_config=yes`  
Other parameters:  

BlockSize: AF_PACKET provides packets to user-space by filling up memory blocks of a specific size, until it either can't fit the next packet into the current block or a timeout is reached. The larger the block, the more packets can be passed to the user at once. BlockSize is in bytes.  

NumBlocks: Number of blocks to allocate in memory. NumBlocks * BlockSize is the total size in memory of the AF_PACKET packet memory region for a single fanout.
BlockTimeoutMillis: If fewer than BlockSize bytes are sniffed by AF_PACKET before this number of milliseconds passes, AF_PACKET provides the current block to users in a less-than-full state.  

FanoutSize: The number of memory regions to fan out to. Total memory usage of AF_PACKET is FanoutSize * MemoryRegionSize, where MemoryRegionSize is BlockSize * NumBlocks. FanoutSize can be considered the number of parallel processes that want to access packet data.  

FanoutID: Integer fanout ID to use when setting socket options. These are globally unique so it can be tuned to avoid conflicts with other processes that use AF_PACKET. If unspecified or 0 an ID will be auto assigned starting with 1.  

NumberOfClients option is used to specify the expected number of clients to connect to the socket.
If option not set, default value is zero.

User: This socket will be owned by the given user, mode 0600. This allows root to provide different sockets with different capabilities to specific users.  
After configuration you’ll see all parameters before saving to file:

```
Generated config: 
[
    {
      "SocketName": "/tmp/testimony.sock"
      , "Interface": "eth0"
      , "BlockSize": 1048576
      , "FrameSize": 16384
      , "NumBlocks": 4
      , "BlockTimeoutMillis": 1000
      , "NumberOfClients": 3,
      , "FanoutSize": 0
      , "User": "stenographer"
    }
]
```

### Stenographer:

```
usage: configuration.py stenographer [-h]
                                     [--stenographer_file_path STENOGRAPHER_FILE_PATH]
                                     [--stenotype_path STENOTYPE_PATH]
                                     [--testimony_socket TESTIMONY_SOCKET]
                                     [--interface INTERFACE] [--port PORT]
                                     [--host HOST] [--cert_path CERT_PATH]
                                     [--fanout_size FANOUT_SIZE]
                                     [--packet_index_dir PACKET_INDEX_DIR]
                                     [--max_packet_files MAX_PACKET_FILES]
                                     [--disk_percentage DISK_PERCENTAGE]

optional arguments:
  -h, --help            show this help message and exit
  --stenographer_file_path STENOGRAPHER_FILE_PATH
  --stenotype_path STENOTYPE_PATH
  --testimony_socket TESTIMONY_SOCKET
  --interface INTERFACE
  --port PORT
  --host HOST
  --cert_path CERT_PATH
  --fanout_size FANOUT_SIZE
  --packet_index_dir PACKET_INDEX_DIR
  --max_packet_files MAX_PACKET_FILES
  --disk_percentage DISK_PERCENTAGE
```

Please, define interface OR testimony_socket. Stenographer will work only if one option is defined.

After configuration you’ll see similar result:

```
Generated config: 
{
    "StenotypePath": "/usr/bin/stenotype",
    "Interface": "wlp3s0",
    "Port": 1234,
    "Host": "127.0.0.1",
    "Flags": [],
    "CertPath": "/etc/stenographer/certs",
    "TestimonySocket": "/tmp/testimony.sock",
    "Threads": [
        {
            "PacketsDirectory": "/data/stenographer/PK0",
            "IndexDirectory": "/data/stenographer/ID0",
            "MaxDirectoryFiles": 30000,
            "DiskFreePercentage": 1
        }
    ]
}
```

### Zeek:

```
usage: configuration.py zeek [-h] [--zeek_file_path ZEEK_FILE_PATH]
                             [--testimony_socket TESTIMONY_SOCKET]
                             [--lb_procs LB_PROCS] [--pin_cpus PIN_CPUS]
                             [--fanout_size FANOUT_SIZE]
                             [--disable_logger DISABLE_LOGGER]

optional arguments:
  -h, --help            show this help message and exit
  --zeek_file_path ZEEK_FILE_PATH
  --testimony_socket TESTIMONY_SOCKET
  --lb_procs LB_PROCS
  --pin_cpus PIN_CPUS
  --fanout_size FANOUT_SIZE
  --disable_logger DISABLE_LOGGER
```

Logger can be disabled if syslog-ng is not installed. Check [Zeek](#Zeek).  
After configuration you’ll see result like this (fanout size was set to 3 (we have 3 workers), testimony socket was set to /tmp/testimony.sock):

```
[logger]
type = logger
host = localhost

[manager]
type = manager
host = localhost

[proxy-1]
type = proxy
host = localhost

[worker-1]
type = worker
host = localhost
interface = /tmp/testimony.sock
lb_method = custom
lb_procs = 1
env_vars = TESTIMONY_FANOUT_ID=0

[worker-2]
type = worker
host = localhost
interface = /tmp/testimony.sock
lb_method = custom
lb_procs = 1
env_vars = TESTIMONY_FANOUT_ID=1

[worker-3]
type = worker
host = localhost
interface = /tmp/testimony.sock
lb_method = custom
lb_procs = 1
env_vars = TESTIMONY_FANOUT_ID=2
```



### Suricata + testimony + stenographer:

```
usage: configuration.py suricata [-h]
                                 [--suricata_file_path SURICATA_FILE_PATH]
                                 [--testimony_lib TESTIMONY_LIB]
                                 [--stenographer_lib STENOGRAPHER_LIB]
                                 [--testimony_socket TESTIMONY_SOCKET]
                                 [--fanout_size FANOUT_SIZE]
                                 [--filename FILENAME] [--pcap_dir PCAP_DIR]
                                 [--cert_dir CERT_DIR]
                                 [--command_pipe PATH_TO_COMMAND_PIPE]
                                 [--before_time BEFORE_TIME]
                                 [--after_time AFTER_TIME]
                                 [--compression COMPRESSION]
                                 [--no_overlapping NO_OVERLAPPING]
                                 [--clean_up CLEAN_UP]
                                 [--cleanup_script CLEANUP_SCRIPT]
                                 [--expiry_time EXPIRY_TIME]
                                 [--min_disk_space_left MIN_DISK_SPACE_LEFT]
```


# Configuration generation with .yaml file
All configurations might be generated with .yaml file in format:
```
testimony:
  testimony_file_path: /etc/testimony.conf
  socket_name: /tmp/testimony.sock
  interface: eth0
  block_size: 1048576
  num_blocks: 16
  fanout_size: 1
  block_timeout_millis: 1000
  user: stenographer

zeek:
  zeek_file_path: /etc/nsm/node.cfg
  testimony_socket: /tmp/testimony.sock
  lb_procs: 1
  pin_cpus: 1/2
  fanout_size: 2

suricata:
  suricata_file_path: /etc/suricata/suricata.yaml
  testimony_lib: /usr/local/lib/source-testimony.so
  ...

stenographer:
  stenographer_file_path: /etc/stenographer/config
  stenotype_path: /usr/bin/stenotype
  ...
```

Where each section contains parameters for each app.

`configuration_file.py` script is used for configuration generation from file.

Example for `configuration.yaml` file: 

```./configuration_file.py --file_path=configuration.yaml```


# Configuration Testing 

Pytest is used to test proper installation of every component:
 Scripts testing is in separate folder, so firstly go to
Reproducible build folder (fetch reproducible build from repository:  
``git clone https://vmalakhatko@bitbucket.org/vmalakhatko/reproducible_build.git
)``
Go to reproducible_build/configuration-script/packages folder:  
`cd reproducible_build/configuration-script/packages`

## Install python requirements:

```
sudo pip3 install *.whl
sudo rpm -ihv *.rpm 
```
These libraries should be installed (pytest, testinfra) properly.

All default values for apps folders and paths are stored in “vars” library:
Example for suricata:
services:
  - suricata

full path to file, user:group, mode:

```
file_paths:
  - ['/usr/bin/suricata', 'root:root', '0777']
  - ['/etc/logrotate.d/suricata', 'root:root', '0644']
  - ['/etc/suricata/suricata.yaml', 'suricata:root', '0600']
  - ['/etc/suricata/classification.config','suricata:root', '0600']
  - ['/etc/suricata/reference.config', 'suricata:root', '0600']

dir_paths:
  - /etc/suricata
  - /var/log/suricata
  - /var/lib/suricata/rules
  - /var/lib/suricata/update

packages:
  - suricata
```

Here are name of systemd service (suricata), file_paths for binary and config files, paths for log, rules, config folders, name of installed package (suricata).

Now you can run pytest command in “configuration-script” to test everything
Or run pytest with specific test file to test specific component:

```
pytest test_stenographer.py 
pytest test_testimony.py
pytest test_suricata.py   
pytest test_zeek.py
```

# Performance increasing script

To increase performance of network interface code, run `increase_interface_performance.sh` script.
It takes 2 arguments: 
```
 -i - interface
 -p - number of ports on your network card
```

This script working with NIC driver and irq options to provide max interface perfomance.

Example of usage: 
`increase_interface_performance.sh -i=eth0 -p=1`
to configure interface `eth0` which is the only interface on network card.

### Last changes
1. Added new script ```test.sh```. This stript checks status of testimony, ZEEK, and testimony plugin for ZEEK. 


