. ../utils.sh
readonly SURICATA_STENOGRAPHER_GIT="https://github.com/MalakhatkoVadym/suricata-stenographer-plugin.git"
parse_args "${@}"

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

readonly SURICATA_TAR_PATH="/root/suricata-${VERSION}.tar.gz"

wget -P "/root/" "https://github.com/OISF/suricata/archive/suricata-${VERSION}.tar.gz" || {
    error "Failed to download Suricata version '${VERSION}'"
}

tar -C "/root/" -xzf "${SURICATA_TAR_PATH}"  || {
    error "Failed extract Suricata version '${VERSION}'"
}

# load_repo "${SURICATA_GIT}" "/root/suricata-src" "${SURICATA_COMMIT}"
load_repo "${LIBHTP_GIT}" "/root/suricata-suricata-${VERSION}/libhtp" "${LIBHTP_COMMIT}"
cd "/root/suricata-suricata-${VERSION}"
./autogen.sh
./configure
load_repo "${SURICATA_STENOGRAPHER_GIT}" "/root/suricata-stenographer/"

cd /root/suricata-stenographer
mkdir build
CPPFLAGS="-I/root/suricata-suricata-${VERSION}/src" make -j`nproc`

cp /root/suricata-stenographer-plugin/suricata-stenographer-plugin.yaml /root/suricata-stenographer/

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog

build_go_rpm suricata-stenographer-plugin.yaml suricata-stenographer-1.0.x86_64.rpm