#!/bin/bash -xe

. ../utils.sh


# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

if [[ -z "${LOCAL_BUILD}" ]]; then

    parse_args "${@}"

    readonly ZEEK_TAR_PATH="${RPM_SOURCES}/zeek-${VERSION}.tar.gz"
    
    if [[ "${VERSION}" == "testimony" ]]; then
        error "This option is obsolete, use separate zeek-testimony plugin build"
    else
        wget -P "${RPM_SOURCES}/" "https://download.zeek.org/zeek-${VERSION}.tar.gz" || {
            error "Failed to download Zeek version '${VERSION}'"
        }

        tar -C "${RPM_BUILD}" -xzf "${ZEEK_TAR_PATH}"  || {
            error "Failed extract Zeek version '${VERSION}'"
        }
    fi
else
    VERSION=local
    readonly ZEEK_TAR_PATH="${RPM_SOURCES}/zeek-${VERSION}.tar.gz"
    ls /root/zeek-local/
    
    cd /root
    tar -czf "${RPM_SOURCES}/zeek-${VERSION}.tar.gz" ./zeek-local
    tar -C "${RPM_BUILD}" -xzf "${ZEEK_TAR_PATH}"  || {
        error "Failed extract Zeek version '${VERSION}'"
    }
    cd /root/zeek
fi

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog
export SOURCE_DATE_EPOCH="$(grep -v "^$" "${RPM_BUILD}/zeek-${VERSION}/CHANGES" | head -1 | cut -d' ' -f3 | date -f- +%s)"
export ZEEK_CONFIGURE_FLAGS="${BUILD_ARGS_TAIL[@]}"

build_rpm zeek.spec