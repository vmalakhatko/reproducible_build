%define _buildhost reproducible 
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}


Name:           suricata-hassh
Version:        %{_version}
Release:        0

Summary:        Open Source Next Generation Intrusion Detection and Prevention Engine
License:        GPL-2.0-only
URL:            https://github.com/MalakhatkoVadym/suricata-hassh
#Source0:        suricata-% # {_version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-root
Requires:       jansson nss libnet libnetfilter_queue pkg-config

%description
The Suricata Engine is an Open Source Next Generation Intrusion Detection and Prevention Engine. This engine is not intended to just replace or emulate the existing tools in the industry, but will bring new ideas and technologies to the field.

# Setup step is disabled because it requres source archive to be available. 
# In case of the usage of git version it is pointless.
%prep
#%setup -q
%build

cd %{_builddir}/suricata-hassh-%{_version}

CFG_FLAGS=(${SURICATA_CONFIGURE_FLAGS})

./autogen.sh
./configure --prefix="%{_prefix}" \
    --sysconfdir="%{_sysconfdir}" \
    --localstatedir="%{_var}" \
    --libdir="%{_libdir}" \
    --enable-gccmarch-native=no \
    ${CFG_FLAGS[@]}

find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

make -j`nproc`

%install

export QA_RPATHS=0x0001

[ "${RPM_BUILD_ROOT}" != "/" ] && rm -rf "${RPM_BUILD_ROOT}"

cd %{_builddir}/suricata-hassh-%{_version}

if [[ "%{_version}" == "testimony" ]]; then
    install -D -m 644 testimony/c/libtestimony.{a,so} -t "${RPM_BUILD_ROOT}/%{_libdir}"
fi

make DESTDIR="${RPM_BUILD_ROOT}" install
make DESTDIR="${RPM_BUILD_ROOT}" install-conf
make DESTDIR="${RPM_BUILD_ROOT}" install-data

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/suricata-hassh-%{_version}/files.list

%files -f %{_builddir}/suricata-hassh-%{_version}/files.list

