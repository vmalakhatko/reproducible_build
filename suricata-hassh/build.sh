#!/bin/bash -xe

. ../utils.sh

# `--` to skip version
parse_args -- "${@}"

# As `version` is not specified in commandline args for this build, it should be set here
VERSION="${SURICATA_HASSH_COMMIT:-latest}"

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

load_repo "${SURICATA_HASSH_GIT}" "${RPM_BUILD}/suricata-hassh-${VERSION}" "${SURICATA_HASSH_COMMIT}" "${SURICATA_HASSH_BRANCH}"
load_repo "${LIBHTP_GIT}" "${RPM_BUILD}/suricata-hassh-${VERSION}/libhtp" "${LIBHTP_COMMIT}"

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog
export SOURCE_DATE_EPOCH="$(head -1 "${RPM_BUILD}/suricata-hassh-${VERSION}/ChangeLog" | rev | cut -d' ' -f1 | rev | date -f- +%s)"
export SURICATA_CONFIGURE_FLAGS="${BUILD_ARGS_TAIL[@]}"

build_rpm suricata-hassh.spec

