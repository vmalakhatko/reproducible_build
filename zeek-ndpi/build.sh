#!/bin/bash -xe

. ../utils.sh

# `--` to skip version
parse_args -- "${@}"

# As `version` is not specified in commandline args for this build, it should be set here
VERSION="${ZEEK_NDPI_COMMIT:-latest}"

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

# Load plugin sources firstly, then load testimony itself
load_repo "${ZEEK_NDPI_GIT}" "${RPM_BUILD}/zeek-ndpi-${VERSION}"

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest commit
export SOURCE_DATE_EPOCH="$(git -C "${RPM_BUILD}/zeek-ndpi-${VERSION}" log -1 --format=%ct)"

# Zeek-Testimony plugin has Zeek both as Build and Runtime dependency, so it have to be installed
yum install --nogpgcheck -y "/deps/${ZEEK_RPM}"

wget --no-check-certificate https://forensics.cert.org/cert-forensics-tools-release-el8.rpm
rpm -Uvh cert-forensics-tools-release*rpm
yum --enablerepo=forensics install -y nDPI-devel

pip3 install GitPython semantic-version
zkg config plugin_dir

build_rpm zeek-ndpi.spec

