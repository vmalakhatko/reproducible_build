%define _buildhost reproducible 
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}

# So here it is excluded manually.

Name:           zeek-ndpi
Version:        %{_version}
Release:        0

Summary:        Zeek-NDI plugin
License:        GPL-2.0-only
URL:            https://github.com/MalakhatkoVadym/zeek-ndpi-plugin
BuildRoot:      %{_tmppath}/%{name}-root
Requires:       zeek
BuildRequires:  zeek

%description
This plugin provides support for ndpi. 

# Setup step is disabled because it requres source archive to be available. 
# In case of the usage of git version it is pointless.
%prep
#%setup -q
%build

cd %{_builddir}/zeek-ndpi-%{_version}

echo "Building Zeek-NDPI plugin"

%define plugin_dir %(zkg config plugin_dir)
echo "plugin dir is %{plugin_dir}"
./configure --install-root="${RPM_BUILD_ROOT}/%{plugin_dir}"

find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

cd build
make -j`nproc`
cd -

%install

[ "${RPM_BUILD_ROOT}" != "/" ] && rm -rf "${RPM_BUILD_ROOT}"

cd %{_builddir}/zeek-ndpi-%{_version}

# Install Zeek-NDPI plugin
cd build
make install
cd -

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/zeek-ndpi-%{_version}/files.list

%files -f %{_builddir}/zeek-ndpi-%{_version}/files.list

