#!/bin/bash -e

readonly IMAGE_NAME="reproducible-builder"
readonly HELP="\
Usage: ${0} <TARGET> ...
    TARGET is either \"suricata\", or \"zeek\", or \"zt\" (Zeek-Testimony plugin)
Formats for targets:
${0} suricata
or
${0} zeek <VERSION> [CONFIGURE-FLAGS]
or
${0} zt --zeek-rpm=<path> [ZT-FLAGS]

                              
For \"zeek\" VERSION should always be a valid version code, because Zeek is downloaded as .tar.gz from official repo.

ZT-FLAGS (\"zt\" target):
  --zeek-rpm=<path>         - set path to pre-built Zeek RPM package, it is used as the build dependency
  --testimony-commit=<hash> - specify testimony commit. If not set, lates is used
  --zt-commit=<hash>        - specify Zeek-Testimony plugin commit. If not set, latest is used

RPM is saved in \"${PWD}/out\" directory
"

function error_and_help() {
    1>&2 echo "${@}"
    echo "${HELP}"
    exit 1
}

case "${1}" in
    --help | -h)
        echo "${HELP}"
        exit 0
    ;;
    suricata | stenographer | suricata-stenographer-plugin | suricata-testimony-plugin | osquery | zeek-agent)
        mkdir -p out
        docker run -it -v "${PWD}/out":/root/dest "${IMAGE_NAME}" ./entrypoint.sh "${@}"
    ;;
    zeek)
        mkdir -p out
        zeek_path_opt="${2}"
        
        if [[ "${zeek_path_opt}" == '--zeek-local-path='* ]]; then
            zeek_path="$(readlink -f ${zeek_path_opt#*=})"
            echo "${zeek_path}"
            docker run -it \
                -v "${PWD}/out":/root/dest -v "${zeek_path}:/root/zeek-local/" \
                -e "LOCAL_BUILD=yes" \
                "${IMAGE_NAME}" ./entrypoint.sh zeek
        else
            docker run -it -v "${PWD}/out":/root/dest "${IMAGE_NAME}" ./entrypoint.sh "${@}"
        fi
    ;;
    testimony)
        mkdir -p out
        testimony_path_opt="${2}"
        
        if [[ "${testimony_path_opt}" == '--testimony-local-path='* ]]; then
            testimony_path="$(readlink -f ${testimony_path_opt#*=})"
            echo "${testimony_path}"
            docker run -it \
                -v "${PWD}/out":/root/dest -v "${testimony_path}:/root/go/src/github.com/google/testimony/" \
                -e "LOCAL_BUILD=yes" \
                "${IMAGE_NAME}" ./entrypoint.sh testimony
        else
            docker run -it -v "${PWD}/out":/root/dest "${IMAGE_NAME}" ./entrypoint.sh testimony
        fi
    ;;
    suricata-hassh)
        mkdir -p out
        docker run -it -v "${PWD}/out":/root/dest "${IMAGE_NAME}" ./entrypoint.sh "${@}"
    ;;
    zt)
        rpm_arg="${2}"
        shift 2
        
        [[ "${rpm_arg}" == '--zeek-rpm='* ]] || {
            error_and_help "Expected '--zeek-rpm=' argument, got ${rpm_arg}"
        }

        # This RPM is forwarded to docker container and used in Zeek-Suricata build
        rpm_path="$(readlink -f ${rpm_arg#*=})"
        rpm_file="$(basename ${rpm_arg#*=})"
        
        mkdir -p out
        docker run -it \
            -v "${PWD}/out":/root/dest -v "${rpm_path}:/deps/${rpm_file}" \
            -e "ZEEK_RPM=${rpm_file}" \
            "${IMAGE_NAME}" ./entrypoint.sh zt "${@}"
    ;;
    zn)
        rpm_arg="${2}"
        shift 2
        
        [[ "${rpm_arg}" == '--zeek-rpm='* ]] || {
            error_and_help "Expected '--zeek-rpm=' argument, got ${rpm_arg}"
        }

        # This RPM is forwarded to docker container and used in Zeek-Suricata build
        rpm_path="$(readlink -f ${rpm_arg#*=})"
        rpm_file="$(basename ${rpm_arg#*=})"
        
        mkdir -p out
        docker run -it \
            -v "${PWD}/out":/root/dest -v "${rpm_path}:/deps/${rpm_file}" \
            -e "ZEEK_RPM=${rpm_file}" \
            "${IMAGE_NAME}" ./entrypoint.sh zn "${@}"
    ;;
    bs-import)
        docker load -i "${2}"
    ;;
    bs-export)
        docker save -o "${2}" "${IMAGE_NAME}"
    ;;
    bs-rebuild)
        [[ "${2}" == "--no-cache" ]] && {
            build_flags="--no-cache"
        }
        docker build ${build_flags} -t "${IMAGE_NAME}" .
    ;;
    dependency)
        mkdir -p out
        docker run -it -v "${PWD}/out":/root/dest "${IMAGE_NAME}" ./entrypoint.sh "${@}"
    ;;
    *)
        error_and_help "Unknown target '${1}'"
    ;;
esac

