#!/bin/bash -e

readonly TARGET="${1}"

function error() {
    1>&2 echo "${@}"
    exit 1
}

echo "Searching in CentOS repos..."
INFO=$(docker run -it centos:centos8@sha256:9e0c275e0bcb495773b10a18e499985d782810e47b4fce076422acb4bc3da3dd yum info "${TARGET}") || {
    error "Package '${TARGET}' not found!"
}

VERSION=$(echo "${INFO}" | grep Version | rev | cut -d' ' -f1 | rev | sed 's/\r//g')
RELEASE=$(echo "${INFO}" | grep Release | rev | cut -d' ' -f1 | rev | sed 's/\r//g')

echo "${TARGET}-${VERSION}-${RELEASE}"

