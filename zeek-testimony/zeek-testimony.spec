%define _buildhost reproducible 
%define clamp_mtime_to_source_date_epoch 1
%define use_source_date_epoch_as_buildtime 1

# Without this option rpmbuild stips and repacks `.a` static libraries and uses non-deterministic timestamps.
# This macro disables such behaviour.
%global __os_install_post %{nil}

# libtestimony is already shipped with here, in RPM, but rpmbuild treats it as an external dependency.
# So here it is excluded manually.
#%define __requires_exclude ^.*testimony.*$

Name:           zeek-testimony
Version:        %{_version}
Release:        0

Summary:        Zeek-Testimony plugin
License:        GPL-2.0-only
URL:            https://github.com/MalakhatkoVadym/zeek-testimony-plugin
BuildRoot:      %{_tmppath}/%{name}-root
Requires:       zeek
BuildRequires:  zeek

%description
This plugin provides native Testimony support for Zeek. Testimony is a single-machine, multi-process architecture for sharing AF_PACKET data across processes. (https://github.com/google/testimony).

# Setup step is disabled because it requres source archive to be available. 
# In case of the usage of git version it is pointless.
%prep
#%setup -q
%build

cd %{_builddir}/zeek-testimony-%{_version}

echo "Building Testimony"

cd testimony/c
make
objcopy --enable-deterministic-archives libtestimony.a
make install

cd -

echo "Building Zeek-Testimony plugin"

%define plugin_dir %(zkg config plugin_dir)
echo "plugin dir is %{plugin_dir}"
./configure --install-root="${RPM_BUILD_ROOT}/%{plugin_dir}"

find . -print0 | xargs -0r touch --no-dereference --date="@${SOURCE_DATE_EPOCH}"

cd build
make -j`nproc`
cd -

%install

[ "${RPM_BUILD_ROOT}" != "/" ] && rm -rf "${RPM_BUILD_ROOT}"

cd %{_builddir}/zeek-testimony-%{_version}

# install -D -m 644 testimony/c/libtestimony.so -t "${RPM_BUILD_ROOT}/%{_libdir}"

# Install Zeek-Testimony plugin
cd build
make install
cd -

# Remove timestamps from all static libs
find "${RPM_BUILD_ROOT}" -type f -name '*.a' -exec objcopy --enable-deterministic-archives {} \;

# Create a list of installed files - required content of future RPM package
find "${RPM_BUILD_ROOT}" ! -type d -o -empty | sed "s~^${RPM_BUILD_ROOT}~~g" > %{_builddir}/zeek-testimony-%{_version}/files.list

%files -f %{_builddir}/zeek-testimony-%{_version}/files.list

