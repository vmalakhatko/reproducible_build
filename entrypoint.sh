#!/bin/bash -xe

# What to build, suricata or zeek
TARGET="${1}"

shift

case "${TARGET}" in
    suricata)
        (cd suricata && exec ./build.sh "${@}")
    ;;
    suricata-hassh)
        (cd suricata-hassh && exec ./build.sh "${@}")
    ;;
    zeek)
        (cd zeek && exec ./build.sh "${@}")
    ;;
    zt)
        (cd zeek-testimony && exec ./build.sh "${@}")
    ;;
    zn)
        (cd zeek-ndpi && exec ./build.sh "${@}")
    ;;
    testimony)
        (cd testimony && exec ./build.sh "${@}")
    ;;
    stenographer)
        (cd stenographer && exec ./build.sh "${@}")
    ;;
    suricata-stenographer-plugin)
        (cd suricata-stenographer-plugin && exec ./build.sh "${@}")
    ;;
    suricata-testimony-plugin)
        (cd suricata-testimony-plugin && exec ./build.sh "${@}")
    ;;
    dependency)
        (cd dependency-downloader && exec ./build.sh "${@}")
    ;;
    zeek-agent)
        (cd zeek-agent && exec ./build.sh "${@}")
    ;;
    *)
        1>&2 echo "Unknown build target '${TARGET}'"
        exit 1
    ;;
esac

echo "Build finished!"
