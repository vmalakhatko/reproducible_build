#!/bin/bash -xe

. ../utils.sh

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

cd /root/rpmbuild/BUILD/
git clone "${OSQUERY_GIT}"

yum install -y binutils elfutils

git clone https://github.com/zeek/zeek-agent-framework
cp -a zeek-agent-framework/zeek-agent "${BUILD_DEST}"/


# Installing latest cmake (3.17)
wget https://github.com/Kitware/CMake/releases/download/v3.17.5/cmake-3.17.5.tar.gz
tar zxvf cmake-3.17.5.tar.gz
pushd cmake-3.17.5/
./bootstrap --datadir=share/cmake
make -j`nproc`
make install
popd

# Downloading toolchain
wget https://github.com/osquery/osquery-toolchain/releases/download/1.1.0/osquery-toolchain-1.1.0-x86_64.tar.xz
tar xvf osquery-toolchain-1.1.0-x86_64.tar.xz -C /usr/local

cd /root/rpmbuild/BUILD/osquery
mkdir build
cd build
cmake -DOSQUERY_TOOLCHAIN_SYSROOT=/usr/local/osquery-toolchain -DPACKAGING_SYSTEM=RPM ..
# OSquery has builtin rpm generating functionality
cmake --build . -j`nproc` --target package
sha256sum *.rpm
cp *.rpm "${BUILD_DEST}"/

