# Common functions and constants for Suricata and Zeek builds.

readonly TESTIMONY_GIT="https://github.com/anakorenko/testimony.git"
readonly TESTIMONY_BRANCH="dev"
# repository"https://github.com/MalakhatkoVadym/testimony.git"

# Git is used only to build `testimony` version, both for Suricata and Zeek.
# For all other versions official repo with source archives is used.
readonly SURICATA_DOWNLOAD_REPO="http://www.openinfosecfoundation.org/download"
readonly SURICATA_GIT="https://github.com/OISF/suricata.git"
readonly SURICATA_BRANCH="master"

readonly SURICATA_HASSH_GIT="https://github.com/MalakhatkoVadym/suricata-hassh.git"
readonly SURICATA_HASSH_BRANCH="hassh-feature-2698-v1"

readonly LIBHTP_GIT="https://github.com/OISF/libhtp"

readonly ZEEK_DOWNLOAD_REPO="https://old.zeek.org/downloads"
readonly ZEEK_GIT="https://github.com/alnyan/zeek.git"
readonly ZEEK_BRANCH="testimony"

readonly ZEEK_TESTIMONY_GIT="https://github.com/MalakhatkoVadym/zeek-testimony-plugin.git"
readonly ZEEK_NDPI_GIT="https://github.com/MalakhatkoVadym/zeek-ndpi-plugin.git"

readonly OSQUERY_GIT="https://github.com/osquery/osquery"

# In case of non-`testimony` version, .tar.gz archive from official repo is downloaded here
readonly RPM_SOURCES="$HOME/rpmbuild/SOURCES"

# Contains either archive content from official repo, or cloned git repo
readonly RPM_BUILD="$HOME/rpmbuild/BUILD"

# Common envitonment variables
export TZ=UTC LC_ALL=C.UTF-8

# `D` flag for determinism - tells ar to strip timestamps
export AR_FLAGS="Dcr"

function error() {
    1>&2 echo "${@}"
    exit 1
}

# There are two supported formats of arguments:
# 1. `<version> [configure_flags]`
# 2. `testimony [--testimony-commit=<hash>] [--zeek-commit=<hash>] [--libhtp-commit=<hash>] [--suricata-commit=<hash>] -- [configure_flags]`
# First is for versions from official repo, second is for `testimony` versions.
# In second case, build options should follow after `--` separator.
# --zeek-commit and --libhtp-commit, --suricata-commit are exclusive, of course.
function parse_args() {
    VERSION="${1}"

    [[ -z "${VERSION}" ]] && {
        error "Version must be set!"
    }

    shift

    [[ "${VERSION}" != "testimony" && "${VERSION}" != "--" ]] && {
        BUILD_ARGS_TAIL=("${@}")
        return
    }

    for i in "${@}"
    do
        case $i in
            --testimony-commit=*)
                TESTIMONY_COMMIT="${i#*=}"
                shift
            ;;
            --suricata-hassh-commit=*)
                SURICATA_HASSH_COMMIT="${i#*=}"
                shift
            ;;
            --libhtp-commit=*)
                LIBHTP_COMMIT="${i#*=}"
                shift
            ;;
            --suricata-commit=*)
                SURICATA_COMMIT="${i#*=}"
                shift
            ;;
            --zt-commit=*)
                ZEEK_TESTIMONY_COMMIT="${i#*=}"
                shift
            ;;
            --zn-commit=*)
                ZEEK_NDPI_COMMIT="${i#*=}"
                shift
            ;;
            --)
                # Stop parsing. Following arguments are treated as configure flags.
                shift
                break
            ;;
            *)
                  echo "Unknown option: ${i#*=}"
                  exit 1
            ;;
        esac
    done

    # Save configure flags
    BUILD_ARGS_TAIL=("${@}")
}

# Loads specific repo from git, checkouts to specific branch and commit
# Args:
# $1 - Repo URL
# $2 - Destination path where to clone
# $3 - Specific commit, optional
# $4 - Specific branch, optional
# $5 - Cloning mode, Should be either "recursive", or not set (non-recursive)
function load_repo() {
    local url="${1}"
    local path="${2}"
    local commit="${3}"
    local branch="${4}"
    local mode="${5}"

    # Clone only latest revision to spead-up build, only if specific commit is not set
    if [[ -z "${commit}" ]]; then
        local depth="--depth 1"
    fi

    if [[ ! -z "${branch}" ]]; then
        git clone -b "${branch}" "${url}" "${path}" ${depth} || {
            error "Failed to clone branch '${branch}' of repo '${url}'"
        }
    else
        git clone "${url}" "${path}" ${depth} || {
            error "Failed to clone repo '${url}'"
        }
    fi 

    if [[ ! -z "${commit}" ]]; then
        cd "${path}"
        git checkout "${commit}" || {
            error "Failed to checkout to commit ${commit}. Check for correctness"
        }

        cd -
    fi

    if [[ "${mode}" == "recursive" ]]; then
        # TODO: Add depth to git submodule

        cd "${path}"
        git submodule update --recursive --init || {
            error "Failed to update submodules"
        }
        cd -
    fi
}

# Invokes rpmbuild with a specific .spec file. Then copies resulted RPMs to dest directory and pronts their hashes
# Args:
# $1 - .spec file
function build_rpm() {
    local spec="${1}"

    rpmbuild -ba --define "_version ${VERSION}" "${spec}"

    sha256sum /root/rpmbuild/RPMS/x86_64/*
    cp /root/rpmbuild/RPMS/x86_64/* "${BUILD_DEST}"/
}

function build_go_rpm() {
    local spec="${1}"
    local rpm_name="${2}"
    
    nfpm pkg -f "${spec}" --packager rpm --target ./build/"${rpm_name}"
    
    sha256sum ./build/*
    cp -r ./build/* "${BUILD_DEST}"/
}

