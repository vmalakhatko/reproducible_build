#!/bin/sh
readonly HELP="\
This script working with NIC driver and irq options to provide max suricata perfomance.
Flags:
 -i - interface 
 -p - number of ports on your network card
Name of NC driver is parsed by get_driver_name function"

for i in "$@"
do
case $i in
	-i=*)
	iface="${i#*=}"
	shift
	;;
	-p=*)
	ports="${i#*=}"
	shift
	;;
	*)
	echo $HELP
	exit 1
	;;
esac
done

get_driver_name () {
	
	f=/sys/class/net/$1
	if [-z $f ] 
	then
	    echo "Error, there is no logical name $1 !"
	fi
	
	local dev=$(basename $f)
	local driver=$(readlink $f/device/driver/module/drivers/*)
	if [ $driver ]; then
	    driver=$(basename $driver)	
	fi
	echo $driver
}

drivername=$(get_driver_name $iface)

echo "Interface is - $iface with $ports - ports on network with $drivername - driver"

ethtool -C "$iface" adaptive-rx on rx-usecs 100 
if [ $? -eq 0 ]; then
	ISSUPPORT=1
else 
	ISSUPPORT=0
fi

echo "Getting NUMA node..."
numa_node=$( cat /sys/class/net/"$iface"/device/numa_node )
if [ $? -eq 0 ]; then
	echo "Done"
else 
	echo "Failed"
fi

echo "Disabeling irqbalance and dhcp tools..."
service irqbalance stop
if [ $? -eq 0 ]; then
	echo "Irqbalance disabled"
else 
	echo "Failed"
fi

if [ $ISSUPPORT -eq 1 ]; then 
	echo "Working with NIC (1)..."
	ethtool -C "$iface" adaptive-rx on rx-usecs 100 
	if [ $? -eq 0 ]; then
		echo "Done"
	else 
		echo "Failed"
	fi

	echo "Working with NIC (2)..."
	ethtool -G "$iface" rx 512
	if [ $? -eq 0 ]; then
		echo "Done"
	else 
		echo "Failed"
	fi

	echo "Disabling pause frames..."
	ethtool -A "$iface" rx off tx off
	if [ $? -eq 0 ]; then
		echo "Done"
	else 
		echo "Failed"
	fi
	
else 
	declare -a arr=("MQ" "RSS" "VMDQ" "InterruptThrottleRate" "FCoE" "vxlan_rx" "LRO" )
	declare -a arrval=("0" "1" "0" "12500" "0" "0" "0")
	
	cnt=0
	for ((cmnd=0; cmnd<"${#arr[@]}"; cmnd++))
	do
		ready=""
		for ((i=0; i<$ports; i++));
		do
			ready=$ready${arrval[$cnt]},
		done
		ready="${ready%,}"
		rs=${arr[$cmnd]%,}
		modprobe $drivername $rs=$ready
		cnt=$cnt+1;
		  
	done	
fi

echo "Disabling offloading functions(2)..."
echo > 1 /proc/sys/net/ipv6/conf/enp0s3/disable_ipv6
if [ $? -eq 0 ]; then
	echo "Done"
else 
	echo "Failed"
fi

echo "Disabling offloading functions(3)..."
ip link set dev "$iface" mtu 1000
if [ $? -eq 0 ]; then
	echo "Done"
else 
	echo "Failed"
fi

echo "Disabling offloading functions(5)..."
ethtool -k "$iface"
if [ $? -eq 0 ]; then
	echo "Done"
else 
	echo "Failed"
fi

echo "Pin interrupts..."
sh set_irq_affinity 1 "$iface"
if [ $? -eq 0 ]; then
	echo "Done"
else 
	echo "Failed"
fi

ip link set "$iface" promisc on arp off up
if [ $? -eq 0 ]; then
	echo "Done"
else 
	echo "Failed"
fi"

echo "Work done"






