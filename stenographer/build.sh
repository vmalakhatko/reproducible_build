#!/bin/bash -xe

. ../utils.sh

readonly SURICATA_TAR_PATH="${RPM_SOURCES}/suricata-${VERSION}.tar.gz"

# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

VERSION=1.0

# For `testimony` version testimony and libhtp should be downloaded separately.
# For other versions libhtp is already included into an archive.

load_repo "${TESTIMONY_GIT}" "/root/go/src/github.com/google/testimony" "${TESTIMONY_COMMIT}"

export PATH=$PATH:/usr/local/go/bin

source ~/.bash_profile

cd /root/go/src/github.com/google/testimony/
make
make install 
mkdir build

git clone -- https://github.com/google/gopacket /root/go/src/github.com/google/gopacket
git clone -- https://github.com/golang/protobuf  /root/go/src/github.com/golang/protobuf
git clone -- https://go.googlesource.com/net  /root/go/src/golang.org/x/net
git clone -- https://github.com/golang/leveldb  /root/go/src/github.com/golang/leveldb
git clone -- https://go.googlesource.com/sys  /root/go/src/golang.org/x/sys
git clone -- https://go.googlesource.com/text  /root/go/src/golang.org/x/text
git clone -- https://github.com/google/uuid  /root/go/src/github.com/google/uuid
git clone -- https://go.googlesource.com/mod  /root/go/src/golang.org/x/mod
git clone -- https://github.com/googleapis/go-genproto  /root/go/src/google.golang.org/genproto
git clone -- https://github.com/MalakhatkoVadym/stenographer.git  /root/go/src/github.com/google/stenographer

# go get github.com/google/stenographer
cd $HOME/go/src/github.com/google/
tar -zcvf /tmp/stenographer.tar.gz stenographer/
cp /tmp/stenographer.tar.gz $HOME/rpmbuild/SOURCES/

cd /root/stenographer

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog
export SOURCE_DATE_EPOCH="$(head -1 "${RPM_BUILD}/suricata-${VERSION}/ChangeLog" | rev | cut -d' ' -f1 | rev | date -f- +%s)"
export SURICATA_CONFIGURE_FLAGS="${BUILD_ARGS_TAIL[@]}"

build_rpm stenographer.spec