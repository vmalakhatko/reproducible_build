#!/bin/bash -xe

. ../utils.sh

readonly SURICATA_TESTIMONY_GIT="https://github.com/MalakhatkoVadym/suricata-testimony-plugin.git"
parse_args "${@}"
# Creates all required directories in `$HOME/rpmbuild`
rpmdev-setuptree

readonly SURICATA_TAR_PATH="/root/suricata-${VERSION}.tar.gz"

wget -P "/root/" "https://github.com/OISF/suricata/archive/suricata-${VERSION}.tar.gz" || {
    error "Failed to download Suricata version '${VERSION}'"
}

tar -C "/root/" -xzf "${SURICATA_TAR_PATH}"  || {
    error "Failed extract Suricata version '${VERSION}'"
}

# load_repo "${SURICATA_GIT}" "/root/suricata-src" "${SURICATA_COMMIT}"
load_repo "${LIBHTP_GIT}" "/root/suricata-suricata-${VERSION}/libhtp" "${LIBHTP_COMMIT}"
cd "/root/suricata-suricata-${VERSION}"
./autogen.sh
./configure
load_repo "${TESTIMONY_GIT}" "/root/testimony-src" "${TESTIMONY_COMMIT}"
cd "/root/testimony-src/c"
make
make install
load_repo "${SURICATA_TESTIMONY_GIT}" "/root/suricata-testimony/"

cd /root/suricata-testimony
mkdir build
CPPFLAGS="-I/root/suricata-suricata-${VERSION}/src" make -j`nproc`

cp /root/suricata-testimony-plugin/suricata-testimony-plugin.yaml /root/suricata-testimony/

# `SOURCE_DATE_EPOCH` is a standard mechanism to achive deterministic builds. Almost all well-known tools (gcc, cmake, ...)
# use this variable as a source of timestamps, versions and other time-related data.
# See https://reproducible-builds.org/docs/source-date-epoch/
# Here it is set to the date of the latest record in the Changelog

build_go_rpm suricata-testimony-plugin.yaml suricata-testimony-1.0.x86_64.rpm
